// Copyright (c) 2019 Tildes contributors <code@tildes.net>
// SPDX-License-Identifier: AGPL-3.0-or-later

function getCookie(cookieName) {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
        var cookiePieces = cookies[i].split("=");
        if (cookieName === cookiePieces[0]) {
            return cookiePieces[1];
        }
    }
    return null;
}

var theme = getCookie("theme");
if (theme) {
    document.body.classList.add("theme-"+theme);
}
